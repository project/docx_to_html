# Docx to HTML Convert Tool

## Table of Contents
- [Project Overview](#project-overview)
- [Features](#features)
- [Post-Installation](#post-installation)
- [How to Use](#how-to-use)
- [Notes](#notes)
- [License](#license)
- [Credits](#credits)

## Project Overview

The **Docx to HTML Convert Tool** module provides a purely front-end solution powered by mammoth.js to convert content from Docx to HTML. Since CKEditor 5, the 'Paste from Word (Office)' feature has been limited in its free version. This module allows content authors to copy content from a Docx file and paste it into any long text field, preserving as much of the original styling as possible.

## Features

- Front-end solution: no files are uploaded to the server.
- Converted HTML content can be pasted into any long text field.
- Preview of converted content rendered with the current front theme.
- Supports the following styling copied from a Docx file:
  - Headings
  - Lists
  - Customizable mapping
  - Tables
  - Footnotes and endnotes
  - Images
  - Bold, italics, underlines, strikethrough, superscript, and subscript
  - Links
  - Line breaks
  - Text boxes
  - Comments
  - [And more to come](https://github.com/mwilliamson/mammoth.js?tab=readme-ov-file#mammoth-docx-to-html-converter)

## Post-Installation

The module includes mammoth.js version 1.8.0, so there is no need to install it separately.

## How to Use

1. Enable the module.
2. Navigate to **Administration > Configuration > Content authoring > DOCX to HTML Converter**, or directly access it via the URL `/docx-to-html`. Note that the 'Access DOCX to HTML Converter' permission is required.
3. Choose a Docx file from your local machine. The converted HTML preview will automatically display below, and a 'Copy the HTML' button will appear to copy the entire converted content with HTML styling into the clipboard.

## Notes

- CKEditor 5 has its own tag and attribute restrictions. Some HTML tags or attributes pasted into the editor might be filtered out (e.g., `H1` tag, `IMG` tag). If this happens, check the editor and format settings.
- When copying HTML codes and pasting them into a Drupal text field, ensure that the target text field and format can sanitize (escape) malicious HTML tags and attributes, such as the `Script` tag.

## License

This project is licensed under the GPLv2 license. See the LICENSE.txt file for more information.

## Credits

This module is powered by [mammoth.js](https://github.com/mwilliamson/mammoth.js).

---

For more information, please visit the [project page](https://www.drupal.org/project/docx_to_html_convert_tool).